#!/bin/bash

function backup() {
    cp -f "/data/${1}" "/backups/${3}_${1}_${2}.bak"
}

if [ -z "${COIN}" ]; then
    echo "COIN environment variable not found"
fi

DATE=$(date '+%Y%m%d_%H%M%S')
backup "wallet.dat" "${DATE}" "${COIN}"
backup "peers.dat" "${DATE}" "${COIN}"
backup "${COIN_CONF_FILE}" "${DATE}" "${COIN}"



