#!/bin/bash

BACKUP_DIR="/backups"
echo "Checking ${BACKUP_DIR} for restore files"
for each in ${BACKUP_DIR}/*.restore ; do
    if [ "${each}" != "/backups/*.restore" ]; then
        DEST_FILE=$(echo "${each}" | sed 's/.*_\(.*\)_.*_.*/\1/')
        DEST="/data/${DEST_FILE}"

        echo "Restoring ${each} to ${DEST}" && \
            cp -f "${each}" "${DEST}" && \
            mv "${each}" "${each}.done"
    fi
done