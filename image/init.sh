#!/bin/bash

INIT_DIR="/init"

echo "Initializing"
for each in ${INIT_DIR}/*.sh ; do
    echo "  Running ${each}"
    bash ${each}
done