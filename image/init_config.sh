#!/bin/bash

INIT_FILE="/backups/conf.init"
_COIN_CONF_FILE="/data/${COIN_CONF_FILE}"

if [ ! -f "${_COIN_CONF_FILE}" ]; then
    echo "${_COIN_CONF_FILE} not found, initializing configuration"
    cp "${INIT_FILE}" "${_COIN_CONF_FILE}"
else
    echo "${_COIN_CONF_FILE} exists"
fi